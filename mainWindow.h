#include "gameArea.h"
#include <gtkmm.h>
#include <gdk/gdkkeysyms.h>
#include <array>

class mainWindow:public Gtk::Window
{
    public:
        mainWindow();
        virtual ~mainWindow();
    protected:
        std::unique_ptr<Gtk::MessageDialog> msgDialog;
        Glib::RefPtr<Gtk::EventControllerKey> controller;

        //widget
        gameArea mArea;
        Gtk::Paned panFrame;
        Gtk::Box boxInfBar,boxLiveImg;
        Gtk::Label labLiveData,labSourceData;
        std::array<Gtk::Image,6> listLive;
        Glib::RefPtr<Gdk::Pixbuf> pixbufLive;

        bool keyUnRelease;

        void game_init();

        //sigal deal function
        void on_source_change(int m_source);
        void on_live_change(int live);
        void on_game_finish();
        bool on_key_press(guint keyval,guint keycode,Gdk::ModifierType state);
        void on_key_release(guint keyval,guint keycode,Gdk::ModifierType state);
        void on_msgDialog_Response(int);
};