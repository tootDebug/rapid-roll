# RapidRoll

[![嘟嘟/RapidRoll](https://gitee.com/tootDebug/rapid-roll/widgets/widget_card.svg?colors=4183c4,ffffff,ffffff,e3e9ed,666666,9b9b9b)](https://gitee.com/tootDebug/rapid-roll) 

#### 介绍

复刻nokia彩球滑梯小游戏

#### 软件架构

基于GTKMM4编写的小游戏    
CMake构建    

#### 安装教程

需要gtkmm4运行库    
`mkdir build-release`    
`cd build-release`    
`cmake .. -DCMAKE_BUILD_TYPE=Release`    
`make`    

#### 使用说明

键盘左右控制小球下落    
遇到尖刺或调出游戏区域损失生命值    

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request