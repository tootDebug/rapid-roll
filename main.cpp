#include <gtkmm/application.h>
#include "mainWindow.h"

int main(int argc,char* argv[])
{
    auto app=Gtk::Application::create("org.toolDebug.RapidRoll");
    return app->make_window_and_run<mainWindow>(argc,argv);
}