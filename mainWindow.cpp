#include "mainWindow.h"
#include <iostream>

mainWindow::mainWindow() :
    labLiveData("生命值"),
    labSourceData("0"),
    keyUnRelease(true)
{
    //window attribute
    set_size_request(400,430);
    set_title("彩球滑梯");

    //label style
    labLiveData.set_halign(Gtk::Align::FILL);
    labLiveData.set_hexpand();
    labSourceData.set_halign(Gtk::Align::FILL);
    labSourceData.set_hexpand();

    //boxLiveImg
    try
    {
        pixbufLive=Gdk::Pixbuf::create_from_file("../live.png");
    }
    catch(...)
    {
        std::cerr<<"ERROR: read live.png file"<<std::endl;
    }
    
    boxLiveImg.set_orientation(Gtk::Orientation::HORIZONTAL);
    boxLiveImg.set_hexpand();
    boxLiveImg.set_halign(Gtk::Align::FILL);
    for(int i=0;i<6;i++)
        listLive[i]=Gtk::Image(pixbufLive);
    for(auto & i:listLive)
        boxLiveImg.append(i);

    //boxInfBar
    boxInfBar.set_orientation(Gtk::Orientation::HORIZONTAL);
    boxInfBar.set_size_request(400,30);
    boxInfBar.set_halign(Gtk::Align::FILL);
    boxInfBar.set_hexpand(true);
    boxInfBar.append(labLiveData);
    boxInfBar.append(boxLiveImg);
    boxInfBar.append(labSourceData);

    //panFrame
    panFrame.set_orientation(Gtk::Orientation::VERTICAL);
    panFrame.set_start_child(boxInfBar);
    panFrame.set_end_child(mArea);
    set_child(panFrame);

    //signal
    mArea.signal_game_finish().connect(sigc::mem_fun(*this,&mainWindow::on_game_finish));
    mArea.signal_live_change().connect(sigc::mem_fun(*this,&mainWindow::on_live_change));
    mArea.signal_source_change().connect(sigc::mem_fun(*this,&mainWindow::on_source_change));

    //key event
    controller=Gtk::EventControllerKey::create();
    controller->signal_key_pressed().connect(sigc::mem_fun(*this,&mainWindow::on_key_press),true);
    controller->signal_key_released().connect(sigc::mem_fun(*this,&mainWindow::on_key_release),true);
    add_controller(controller);
}


mainWindow::~mainWindow() {}

void mainWindow::on_source_change(int m_source)
{
    labSourceData.set_text(Glib::ustring().format("分数: ",m_source));
}

void mainWindow::on_live_change(int m_live)
{
    if(m_live>0)
        boxLiveImg.remove(listLive[m_live]);
}

void mainWindow::on_game_finish()
{
    msgDialog.reset(new Gtk::MessageDialog(*this,"游戏结束"));
    msgDialog->set_secondary_text(Glib::ustring("游戏得分: ")+labSourceData.get_text());
    msgDialog->add_button("退出",2);
    msgDialog->set_modal(true);
    msgDialog->set_hide_on_close(true);
    msgDialog->signal_response().connect(sigc::mem_fun(*this,&mainWindow::on_msgDialog_Response));
    msgDialog->show();
}

bool mainWindow::on_key_press(guint keyval,guint keycode,Gdk::ModifierType state)
{
    if(keyval!=0 && keyUnRelease)
    {
        keyUnRelease=false;
        switch (keyval)
        {
            case GDK_KEY_Left:
                mArea.mKeyDir=gameArea::keyDirection::Left;
                break;
            case GDK_KEY_Right:
                mArea.mKeyDir=gameArea::keyDirection::Right;
                break;
            case GDK_KEY_Up:
                mArea.mKeyDir=gameArea::keyDirection::Up;
                break;
            case GDK_KEY_Down:
                mArea.mKeyDir=gameArea::keyDirection::Down;
                break;
            case GDK_KEY_KP_Enter:
                mArea.mKeyDir=gameArea::keyDirection::Enter;
                break;
            default:
                std::cout<<"Unknow Key value: "<<keyval<<std::endl;
        }
    }
    return false;
}

void mainWindow::on_key_release(guint keyval,guint keycode,Gdk::ModifierType state)
{
    keyUnRelease=true;
    mArea.mKeyDir=gameArea::keyDirection::None;
}

void mainWindow::on_msgDialog_Response(int rep)
{
    switch (rep)
    {
        case 2:
            msgDialog->close();
            this->hide();
            break;
        default:
            msgDialog->close();
            game_init();
    }
}

void mainWindow::game_init()
{
    for(auto & i:listLive)
        boxLiveImg.append(i);
    
    labSourceData.set_text(Glib::ustring().format("分数: ",0));
    mArea.gameAreaInit();
}